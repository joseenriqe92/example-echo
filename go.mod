module github.com/joseenriqe97/echo-example

go 1.15

require (
	github.com/ansel1/merry v1.5.1
	github.com/doug-martin/goqu v5.0.0+incompatible
	github.com/doug-martin/goqu/v9 v9.9.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.8.0
	github.com/sethvargo/go-envconfig v0.3.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
)
