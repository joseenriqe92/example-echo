package vo

type User struct {
	ID   int    `json:"Id"`
	User string `json:"User"`
	Text string `default:"Texto predeterminado"`
}
