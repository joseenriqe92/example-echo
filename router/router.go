package router

import (
	"github.com/joseenriqe97/echo-example/api"

	"github.com/labstack/echo"
)

func New() *echo.Echo {
	// create a new echo instance
	e := echo.New()

	//create groups

	//set main routes
	api.MainGroup(e)
	return e
}
