package config

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq" // here
	"github.com/sethvargo/go-envconfig"
)

//Sqldb Conexión base de datos
var Sqldb *sql.DB

// GlobalConfig Config system
var GlobalConfig databaseConfig

type databaseConfig struct {
	Host     string `env:"DATABASE_HOST"`
	Port     int    `env:"DATABASE_PORT,default=5432"`
	User     string `env:"DATABASE_USER"`
	Password string `env:"DATABASE_PASSWORD"`
	DbName   string `env:"DATABASE_DB_NAME"`
}

// PgConn the connection string to the pg database
/* func PgConn() string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		GlobalConfig.Host, GlobalConfig.Port, GlobalConfig.User, GlobalConfig.Password, GlobalConfig.DbName)
}

// ConnectDatabase  conecta a la base de datos
func ConnectDatabase() error {
	psqlInfo := PgConn()
	var err error
	Sqldb, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		return merry.Wrap(err)
	}

	err = Sqldb.Ping()
	if err != nil {
		return merry.Wrap(err)
	}
	return nil
}
*/
// GetConfig reads the Envvars for config
func GetConfig() error {
	ctx := context.Background()
	err := envconfig.Process(ctx, &GlobalConfig)
	return err
}
