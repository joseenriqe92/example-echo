package handlers

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/ansel1/merry"
	"github.com/doug-martin/goqu/v9"
	"github.com/joseenriqe97/echo-example/config"
	"github.com/joseenriqe97/echo-example/vo"
	"github.com/labstack/echo"
)

var Sqldb *sql.DB
var userTableGoqu = goqu.T("client")

func PgConn() string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.GlobalConfig.Host, config.GlobalConfig.Port, config.GlobalConfig.User, config.GlobalConfig.Password, config.GlobalConfig.DbName)
}

// ConnectDatabase  conecta a la base de datos
func ConnectDatabase() error {
	psqlInfo := PgConn()
	var err error
	Sqldb, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		return merry.Wrap(err)
	}

	err = Sqldb.Ping()
	if err != nil {
		return merry.Wrap(err)
	}
	return nil
}

func GetUser(c echo.Context) error {
	var (
		ID   int
		Name string
	)
	queryString, _, _ := goqu.From(userTableGoqu).
		Select(userTableGoqu.Col("id"), userTableGoqu.Col("name")).ToSQL()
	rows, err := Sqldb.Query(queryString)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Zero rows found")
		} else {
			panic(err)
		}
	}
	userCompany := make([]vo.User, 0)
	for rows.Next() {
		if err := rows.Scan(&ID, &Name); err != nil {
			log.Fatal(err)
		}
		/* 	vo.User{
			ID:   ID,
			User: Name,
			Text: "SQL desde desde mi base de datos",
		} */
		user := vo.User{
			ID:   ID,
			User: Name,
			Text: "Successful",
		}
		userCompany = append(userCompany, user)

	}
	return c.JSON(http.StatusOK, userCompany)
}
