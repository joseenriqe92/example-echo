package main

import (
	"fmt"

	"github.com/joseenriqe97/echo-example/config"
	"github.com/joseenriqe97/echo-example/handlers"
	"github.com/joseenriqe97/echo-example/router"
)

func main() {
	// Inicializamos la nueva instancia de echo.

	err := config.GetConfig()
	if err != nil {
		panic(err)
	}
	handlers.ConnectDatabase()
	fmt.Println(config.GlobalConfig.Host)
	e := router.New()
	e.Logger.Fatal(e.Start(":8000"))
}
