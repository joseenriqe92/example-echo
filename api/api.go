package api

import (
	"github.com/joseenriqe97/echo-example/handlers"
	"github.com/labstack/echo"
)

func MainGroup(e *echo.Echo) {
	// Route / to handler function

	e.GET("/users", handlers.GetUser)
	// e.POST("/cats", handlers.AddCat)

}

/* func AdminGroup(g *echo.Group) {
	g.GET("/main", handlers.MainAdmin)
} */
